<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Send Form</title>
</head>
<body>
    <form method="post" action="{{route('send')}}">
        @csrf
        <span>Receiver Number</span>
        <input type="text" name="receiver_number" value="{{old('receiver_number')}}">
        <span>Message</span>
        <input type="text" size="50" name="message" value="{{old('message')}}">
        <input type="submit" value="Send">
    </form>
    <br>
    <div>
        (Note: Receiver number format should be one of following: 9123236908, 09123236908, 989123236908, 00989123236908)
    </div>
    <br>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <br>
    @if (session('result'))
        <div class="alert alert-success">
            Result: {{session('result')}}
        </div>
    @endif
</body>
</html>