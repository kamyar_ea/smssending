<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// showing HTML form of sending sms
Route::get('/','SmsController@sendForm')->name('sendForm');

// sending sms using one of APIs
Route::post('/send','SmsController@send')->name('send');
