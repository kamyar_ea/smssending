<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SmsSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // receiver number should follow this patterns: 9123236908, 09123236908, 989123236908, 00989123236908
        return [
            'receiver_number' => ['required','regex:/^(0098|98|0)?9\d{9}$/'],
            'message' => 'required|string',
        ];

    }
}
