<?php

namespace App\Http\Controllers;

use App\Http\Requests\SmsSendRequest;
use App\Libraries\SmsServices\SmsServiceInterface;

class SmsController extends Controller
{

    public function sendForm()
    {

        // showing HTML form of sending sms
        return view('send_form');

    }

    public function send(SmsSendRequest $request,SmsServiceInterface $sms)
    {

        // data which were validated by SmsSendRequest validation rules
        $validated = $request->validated();

        $receiver_number = $validated['receiver_number'];
        $message = $validated['message'];

        // sending sms by the selected SMS_API_TYPE environment variable
        $result = $sms->sendSms($receiver_number,$message);

        // returning to HTML sending form with the result of server
        return redirect()->route('sendForm')->with('result',$result);

    }
    
}
