<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        switch (env('SMS_API_TYPE')){

            case 'kavehnegar':
                $sms_sender_class = 'App\Libraries\SmsServices\KavehNegar\KavehNegarService';
                break;

            case 'ghasedak':
                $sms_sender_class = 'App\Libraries\SmsServices\Ghasedak\GhasedakService';
                break;

            case 'mellipayamak':
                $sms_sender_class = 'App\Libraries\SmsServices\MelliPayamak\MelliPayamakService';
                break;

            default:
                $sms_sender_class = 'App\Libraries\SmsServices\KavehNegar\KavehNegarService';

        }

        $this->app->bind(
            'App\Libraries\SmsServices\SmsServiceInterface',
            $sms_sender_class
        );
        
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
