<?php

namespace App\Libraries\SmsServices\KavehNegar;

use App\Libraries\SmsServices\SmsServiceInterface;
use Kavenegar\KavenegarApi;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;

class KavehNegarService implements SmsServiceInterface
{

    public function sendSms($receiver_number,$message)
    {

        $api_key = env('KAVEHNEGAR_API_KEY');
        $sender_number = '';

        try{
            $api = new KavenegarApi($api_key);
            $receptor = array($receiver_number);
            $result = $api->Send($sender_number,$receptor,$message);
            if($result){
                return $result;
            }
        }
        catch(ApiException $e){
            return $e->errorMessage();
        }
        catch(HttpException $e){
            return $e->errorMessage();
        }

    }

}