<?php

namespace App\Libraries\SmsServices;

interface SmsServiceInterface
{

    public function sendSms($receiver_number,$message);

}