<?php

namespace App\Libraries\SmsServices\Ghasedak;

use App\Libraries\SmsServices\SmsServiceInterface;

class GhasedakService implements SmsServiceInterface
{

    public function sendSms($receiver_number,$message)
    {

        $api_key = env('GHASEDAK_API_KEY');
        $sender_number = '';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.ghasedak.io/v2/sms/send/simple",
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "message=$message&linenumber=$sender_number&Receptor=$receiver_number&=",
            CURLOPT_HTTPHEADER => array(
                "apikey: $api_key",
            ),
            CURLOPT_RETURNTRANSFER => true
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return $err;
        } else {
            return $response;
        }

    }

}