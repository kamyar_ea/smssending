<?php

namespace App\Libraries\SmsServices\MelliPayamak;

use App\Libraries\SmsServices\SmsServiceInterface;
use Melipayamak\MelipayamakApi;

class MelliPayamakService implements SmsServiceInterface
{

    public function sendSms($receiver_number,$message)
    {

        $username = env('MELLIPAYAMAK_USERNAME');
        $password = env('MELLIPAYAMAK_PASSWORD');
        $sender_number = '';

        try{
            $api = new MelipayamakApi($username,$password);
            $sms = $api->sms();
            $response = $sms->send($receiver_number,$sender_number,$message);
            return $response;
        }catch(\Exception $e){
            return $e->getMessage();
        }

    }

}